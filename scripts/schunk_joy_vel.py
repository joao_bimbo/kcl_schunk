#!/usr/bin/env python
import os
import rospy
import roslib
from numpy import array
roslib.load_manifest('kcl_schunk')
from std_msgs.msg import String
from std_msgs.msg import Float64MultiArray
from control_msgs.msg import FollowJointTrajectoryAction,FollowJointTrajectoryActionGoal,JointTolerance,FollowJointTrajectoryGoal
from trajectory_msgs.msg import JointTrajectoryPoint
from kcl_schunk.srv import FwdKin_srv,InvKin_srv 
from sensor_msgs.msg import JointState,Joy
from geometry_msgs.msg import Pose,PoseStamped
from brics_actuator.msg import JointVelocities,JointValue
import tf

import actionlib
from copy import copy
import sensor_msgs
import numpy
import tf_conversions
from geometry_msgs.msg._Quaternion import Quaternion
import math
import ros
import code
import threading
import time


class schunk_joy_vel:
      
    def js_cb(self,msg):
        self.joint_state.data=msg.position;        
        return
    def array_to_pose(self,trans,rot):
        pose=Pose()
        pose.position.x=trans[0];
        pose.position.y=trans[1];
        pose.position.z=trans[2];
        pose.orientation.w=rot[3];
        pose.orientation.x=rot[0];
        pose.orientation.y=rot[1];
        pose.orientation.z=rot[2];
        return pose
    def pose_to_matrix(self,pose):        
        m = numpy.zeros(shape = (4,4))
        
        m=tf.transformations.quaternion_matrix([pose.orientation.x,pose.orientation.y,pose.orientation.z,pose.orientation.w])
        m[0:3,3]=numpy.array([pose.position.x,pose.position.y,pose.position.z])        
        
        return m;
    
    def cart_to_spher(self,xyz):
        
        r=numpy.linalg.norm(xyz);
        c=numpy.array([r, math.acos(xyz[2]/(r)),math.atan2(xyz[1],xyz[0])])
#         if(c[2]>math.pi):
#             c[0]=-c[0];
#             c[2]=c[2]-math-pi      
        return c
    
    def spher_to_cart(self,rtp):
        return numpy.array([rtp[0]*math.sin(rtp[1])*math.cos(rtp[2]), rtp[0]*math.sin(rtp[1])*math.sin(rtp[2]), rtp[0]*math.cos(rtp[1])])

    def quat_between_vectors(self,v1,v2):        
        axis=numpy.cross(v1,v2)        
        q=[axis[0],axis[1], axis[2], math.sqrt(
                                               (v1[0]*v1[0]+v1[1]*v1[1]+v1[2]*v1[2])
                                               *
                                               (v1[0]*v1[0]+v1[1]*v1[1]+v1[2]*v1[2])
                                               +
                                               (v1[0]*v2[0]+v1[1]*v2[1]+v1[2]*v2[2]))]
                   
        q=q/numpy.linalg.norm(q)
        
        #q.xyz = a;
        #q.w = sqrt((v1.Length ^ 2) * (v2.Length ^ 2)) + dotproduct(v1, v2)
        return q    
        
    def getPose(self,angles):         
        rospy.wait_for_service('schunk_fwd_kin')
        srv1 = rospy.ServiceProxy('schunk_fwd_kin', FwdKin_srv)
        rsp = srv1(angles)        
        pose=Pose()
        pose=rsp.pose;
        return pose;
    def getIK(self,pose):
        rospy.wait_for_service('schunk_inv_kin')        
        srv2 = rospy.ServiceProxy('schunk_inv_kin', InvKin_srv)
        try:
            rsp = srv2(pose)
            return rsp.joint_angles
        except rospy.ServiceException:
            print "Error in ik"
            stay=list()
            stay.append(self.joint_state)
            return stay        
    def getIK_best(self,pose):            
        now=self.joint_state.data;
        diff=1000;
        best=self.joint_state
        try:
            sols = self.getIK(pose)
            for j in range(0, len(sols)):
                    #d = numpy.linalg.norm(numpy.array(now) - numpy.array(sols[j].data))
                    #maximum distance travelled by joint
                    sol=numpy.array(sols[j].data)
                    d = max(numpy.absolute(numpy.array(now) - sol))
                    #away from joint limits
                    if(max(numpy.absolute([sol[1],sol[3],sol[5]]))>2.08439):
                        d=1000;
                    if d < diff:
                        diff = d;
                        best = sols[j]            
            #print(sols)
            if(len(sols)>0):
                return best
            else:
                return false
        except rospy.ROSInterruptException:
            print "Error in IK"
            
            
    def controller(self,jangles):
        error=jangles.data-array(self.joint_state.data);
        if(numpy.linalg.norm(error)<1):
            Kp=0.5;
            for i in range(0,6):
                self.vels.velocities[i].value=error[i]*Kp;
                print(error)
            self.pub1.publish(self.vels)
        else:
            return
        
        
    def move_RCM(self,d):
        cur_pose=self.getPose(self.joint_state)
        iTb_r=numpy.linalg.inv(self.Tb_r)
        
        Tee_b=self.pose_to_matrix(cur_pose)
        T_ee_r=numpy.matrix(iTb_r)*numpy.matrix(Tee_b);
        cur_sph=self.cart_to_spher(T_ee_r[0:3,3])
            
            #Do changes according to joystick
        s=numpy.sign(cur_sph[0]);
        s=1                                  
        cur_sph[0]=cur_sph[0]+(s*d[1]*self.deltar);
        s=numpy.sign(cur_sph[1]);
        cur_sph[1]=cur_sph[1]+(s*d[2]*self.deltaa);
        s=numpy.sign(cur_sph[2]);                        
        cur_sph[2]=cur_sph[2]+(s*d[3]*self.deltaa);            
        xyz=self.spher_to_cart(cur_sph)            
                    
        v1=[-T_ee_r[0,2],-T_ee_r[1,2],-T_ee_r[2,2]];
        #v1=v1/numpy.linalg.norm(v1)
        v2=xyz/numpy.linalg.norm(-xyz)
        q=self.quat_between_vectors(v1,v2)
        #new_pose_r=self.array_to_pose(xyz, q)
        
        Trr=tf.transformations.quaternion_matrix(q)            
    
        Tfinal_r=Trr*T_ee_r
        
        
        #Tfinal_r=Trr2*T_ee_r;
      
        #Tfinal_r[0:3,3]=numpy.transpose(numpy.matrix(xyz[0:3]))
        
        xyz2=Tfinal_r[0:3,0:3]*numpy.transpose(numpy.matrix([0,0,-cur_sph[0]]))
        
        Tfinal_r[0:3,3]=xyz2[0:3]
        
        #changed here from 'rxyz'
        Trr2=tf.transformations.euler_matrix(d[4]*self.deltaa,d[5]*self.deltaa, 0, 'sxyz') 
        Tfinal2_r=Trr2*Tfinal_r;
        
        Pee_b=self.Tb_r*Tfinal2_r;
        
        q_next=tf.transformations.quaternion_from_matrix(Pee_b);
        new_pose=self.array_to_pose([Pee_b[0,3], Pee_b[1,3], Pee_b[2,3]], q_next)
        
                    
        jangles=self.getIK_best(new_pose)
        n_ang=array(jangles.data)
        n_ang[5]=n_ang[5]+d[6]*self.deltaa;
       
        jangles.data=n_ang;
        
        self.controller(jangles)
    
        
    
    def save_rcm(self):
        cur_pose=self.getPose(self.joint_state)
        trans=[cur_pose.position.x, cur_pose.position.y,cur_pose.position.z];
        rot=[cur_pose.orientation.x, cur_pose.orientation.y,cur_pose.orientation.z,cur_pose.orientation.w]
        Tb_ee=numpy.matrix(self.transf.fromTranslationRotation(trans, rot))
        transh=numpy.array([[cur_pose.position.x, cur_pose.position.y,cur_pose.position.z,1]])
        
        I=numpy.identity(4)
        I[2,3]=0.2;
        print(I)        
        self.Tb_r=Tb_ee*I
        
        
        
        #iTb_r=numpy.linalg.inv(self.Tb_r)
        
        #print(iTb_r.shape)
        #print(transh.shape)
        #print(numpy.linalg.inv(self.Tb_r))
        #print(transh)
        #print(numpy.linalg.inv(self.Tb_r)*numpy.transpose(transh));        
        
        

    def move_cartesian(self,d):
        cur_pose=self.getPose(self.joint_state)
        T_ee_r=numpy.matrix(self.pose_to_matrix(cur_pose))
        delta_pose=tf.transformations.euler_matrix(d[4]*self.deltaa,d[5]*self.deltaa, d[6]*self.deltaa, 'sxyz')
            
        Pee_b=T_ee_r
        Pee_b[0:3,0:3]=delta_pose[0:3,0:3]*T_ee_r[0:3,0:3]            
            
        Pee_b[0:3,3]=numpy.transpose(numpy.matrix([T_ee_r[0,3]+d[1]*self.deltar, T_ee_r[1,3]+d[2]*self.deltar, T_ee_r[2,3]+d[3]*self.deltar]))
            
            
            
        q_next=tf.transformations.quaternion_from_matrix(Pee_b);
        new_pose=self.array_to_pose([Pee_b[0,3], Pee_b[1,3], Pee_b[2,3]], q_next)
            
        jangles=self.getIK_best(new_pose)
        self.controller(jangles)


    def joy_cb(self,msg):
        b1=msg.buttons[10];
        b2=msg.buttons[11];
        bsel=msg.buttons[0];
        d1=msg.axes[0];
        d2=msg.axes[1];
        d3=msg.axes[2];
        d4=msg.axes[3];
        d5=msg.buttons[4]-msg.buttons[6];
        d6=msg.buttons[5]-msg.buttons[7];        
        d=array([0,d1,d2,d3,d4,d5,d6]);
        if bsel==1:
            rospy.sleep(0.1)
            self.save_rcm();

        elif b1==1:
            self.move_cartesian(d)
            
        elif b2==1:
            self.move_RCM(d)        
            
        else:
            self.pub1.publish(self.stop)
                    
    
    
    def __init__(self):
        rospy.init_node('schunk_joy', anonymous=True)   
        #self.listener = tf.TransformListener()     
        #self.transf=tf.TransformerROS()
        self.sub1=rospy.Subscriber("/joint_states",JointState,self.js_cb)        
        self.sub2=rospy.Subscriber("/joy",Joy,self.joy_cb, queue_size=1)
        self.pub1=rospy.Publisher("/arm_controller/command_vel",JointVelocities,queue_size=2)
        self.transf=tf.TransformerROS()

        self.joint_state=Float64MultiArray();
        self.deltar=0.01
        self.deltaa=0.1
        self.speed=0.1;       
        self.vels = JointVelocities()
        self.stop = JointVelocities()

        vel=JointValue()
        joint_names=['arm_1_joint', 'arm_2_joint', 'arm_3_joint', 'arm_4_joint', 'arm_5_joint', 'arm_6_joint'];
        for i in range(0,6):
            vel.timeStamp=rospy.Time.now()
            vel.joint_uri=joint_names[i]
            vel.unit="rad"
            self.vels.velocities.append(copy(vel))
            self.stop.velocities.append(copy(vel))
        
        
    def run(self):        
        r = rospy.Rate(2)
        while not rospy.is_shutdown():
            r.sleep()

 




if __name__ == '__main__':
    print("joao")
    joy_node=schunk_joy_vel();
    joy_node.run()
    
    
    rospy.spin()