#!/usr/bin/env python
import roslib; roslib.load_manifest('schunk_kcl')
import rospy
from copy import copy
from trajectory_msgs.msg import JointTrajectory,JointTrajectoryPoint
from pr2_controllers_msgs.msg import JointTrajectoryControllerState
import numpy


def initial_callback(data):
    #rospy.loginfo(rospy.get_caller_id() + "I heard %s", data)
    print(data)
    traj=JointTrajectory()
    p1=JointTrajectoryPoint()
    p1=data.actual;
    p=p1;
    va=list(p.velocities)
    va[5]=0.05
    p.velocities=tuple(va)
    
    traj.joint_names=['arm_1_joint','arm_2_joint','arm_3_joint','arm_4_joint','arm_5_joint','arm_6_joint'];
    for i in range(0,10):
		#pa=numpy_array(p1.positions)		
		pa=list(p.positions)
		pa[5]=pa[5]+(0.01)		
		p.positions=tuple(pa)
		print(str(p.positions[5]) + " " + str(i))
		pp=copy(p)
		traj.points.append(pp)
		
    pub = rospy.Publisher('/arm_controller/command', JointTrajectory)
    pub.publish(traj)

if __name__ == '__main__':
    rospy.init_node('schunk_kcl', anonymous=True)
    rospy.Subscriber("/arm_controller/state", JointTrajectoryControllerState, initial_callback)       
    rospy.spin()



