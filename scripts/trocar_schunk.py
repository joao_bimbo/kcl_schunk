#!/usr/bin/env python
import os
import rospy
import roslib
from numpy import array
roslib.load_manifest('kcl_schunk')
from std_msgs.msg import String
from std_msgs.msg import Float64MultiArray
from control_msgs.msg import FollowJointTrajectoryAction,FollowJointTrajectoryActionGoal,JointTolerance,FollowJointTrajectoryGoal
from trajectory_msgs.msg import JointTrajectoryPoint
from kcl_schunk.srv import FwdKin_srv,InvKin_srv 
from sensor_msgs.msg import JointState,Joy
from geometry_msgs.msg import Pose,PoseStamped
from kcl_schunk.msg import TrocarBaseState
import tf
from brics_actuator.msg import JointVelocities,JointValue


import actionlib
from copy import copy
import sensor_msgs
import numpy
import tf_conversions
from geometry_msgs.msg._Quaternion import Quaternion
import math
import ros
import code
import threading
import time

class trocar_schunk:
    def getPose(self,angles):         
        rospy.wait_for_service('schunk_fwd_kin')
        srv1 = rospy.ServiceProxy('schunk_fwd_kin', FwdKin_srv)
        rsp = srv1(angles)        
        pose=Pose()
        pose=rsp.pose;
        return pose;
    def getIK(self,pose):
        rospy.wait_for_service('schunk_inv_kin')        
        srv2 = rospy.ServiceProxy('schunk_inv_kin', InvKin_srv)
        try:
            rsp = srv2(pose)
            return rsp.joint_angles
        except rospy.ServiceException:
            print "Error in ik"
            stay=list()
            stay.append(self.joint_state)
            
            return stay
    
    def pose_to_matrix(self,pose):        
        m = numpy.zeros(shape = (4,4))
        
        m=tf.transformations.quaternion_matrix([pose.orientation.x,pose.orientation.y,pose.orientation.z,pose.orientation.w])
        m[0:3,3]=numpy.array([pose.position.x,pose.position.y,pose.position.z])        
        
        return m;        
        
    def js_cb(self,msg):
        self.joint_state.data=msg.position;
        return
    def trocar_cb(self,msg):
        print(self.Tb_r)
        if(self.is_set==True):
            T_trc_insrt=self.pose_to_matrix(msg.base_pose)
            T_trc_w=self.Tb_r*T_trc_insrt
            
            q_next=tf.transformations.quaternion_from_matrix(T_trc_w);
            new_pose=self.array_to_pose([T_trc_w[0,3], T_trc_w[1,3], T_trc_w[2,3]], q_next)
            jangles=self.getIK_best(new_pose)
            print(jangles)
            self.controller(jangles)
    
    def controller(self,jangles):
        error=jangles.data-array(self.joint_state.data);
        if(numpy.linalg.norm(error)<1):
            Kp=0.5;
            for i in range(0,6):
                self.vels.velocities[i].value=error[i]*Kp;
                print(error)
            self.pub1.publish(self.vels)
        else:
            return
               
                     


    
    def __init__(self):
        rospy.init_node('schunk_joy', anonymous=True)   
        #self.listener = tf.TransformListener()     
        self.is_set=False

        #self.transf=tf.TransformerROS()
        self.sub1=rospy.Subscriber("/joint_states",JointState,self.js_cb)        
        self.pub1=rospy.Publisher("/arm_controller/command_vel",JointVelocities,queue_size=2)
        self.transf=tf.TransformerROS()
        self.joint_state=Float64MultiArray();  


        self.set_insertion()
    
    def set_insertion(self):                
        cur_pose=self.getPose(self.joint_state)
       
        trans=[cur_pose.position.x, cur_pose.position.y,cur_pose.position.z];
        rot=[cur_pose.orientation.x, cur_pose.orientation.y,cur_pose.orientation.z,cur_pose.orientation.w]
        Tb_ee=numpy.matrix(self.transf.fromTranslationRotation(trans, rot))
        transh=numpy.array([[cur_pose.position.x, cur_pose.position.y,cur_pose.position.z,1]])
        
        I=numpy.identity(4)
        I[2,3]=0.2;
        print(I)        
        self.Tb_r=Tb_ee*I
        self.is_set=True
        self.sub1=rospy.Subscriber("/sf_base_controller/state",TrocarBaseState,self.trocar_cb)
          




if __name__ == '__main__':
    print("schunk_trocar")
    schunk_trocar_node=trocar_schunk(); 
    rospy.sleep(0.1)   
    schunk_trocar_node.set_insertion()
    rospy.sleep(1)
    rospy.spin();
    
    
    