#!/usr/bin/env python
import rospy
from std_msgs.msg import String
from std_msgs.msg import Float64MultiArray
from control_msgs.msg import FollowJointTrajectoryAction,FollowJointTrajectoryActionGoal,JointTolerance,FollowJointTrajectoryGoal
from trajectory_msgs.msg import JointTrajectoryPoint
from kcl_schunk.srv import FwdKin_srv,InvKin_srv 
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Pose
import tf

import actionlib
from copy import copy
import sensor_msgs
import numpy
import tf_conversions
from geometry_msgs.msg._Quaternion import Quaternion
import math
import ros


class schunk_traj:
    def create_action(self):                    
        self.traj_goal.trajectory.header.stamp=rospy.Time.now()
        self.traj_goal.trajectory.joint_names=['arm_1_joint', 'arm_2_joint', 'arm_3_joint', 'arm_4_joint', 'arm_5_joint', 'arm_6_joint'];
        
        tol = JointTolerance()    
        for i in range(0,6):
            tol.name=self.traj_goal.trajectory.joint_names[i];
            tol.acceleration=0.02
            tol.velocity=0.02
            tol.position=0.001
            self.traj_goal.path_tolerance.append(copy(tol));
            self.traj_goal.goal_tolerance.append(copy(tol));
            
            
        
        return True
    
    def add_point(self,array_pose,time):
        a=JointTrajectoryPoint()
        
        self.point.positions=copy(array_pose.data);        
        self.point.time_from_start=rospy.Duration.from_sec(time)
        self.traj_goal.trajectory.points.append(copy(self.point))
        
        #self.traj_goal.path_tolerance.append(self.tol)
        return True;
    def call_action(self):        
        self.act_client.wait_for_server()
        now=rospy.Time.now()        
        self.act_client.send_goal(self.traj_goal)        
        self.act_client.wait_for_result(rospy.Duration.from_sec(10.0));        
        print(str(self.act_client.get_result()) + " TIME: " + str((rospy.Time.now()-now).to_sec()))
    def __init__(self):
        #self.act_client = actionlib.SimpleActionClient('/arm/joint_trajectory_controller/follow_joint_trajectory',FollowJointTrajectoryAction)
        self.act_client = actionlib.SimpleActionClient('/arm_controller/follow_joint_trajectory',FollowJointTrajectoryAction)
        self.traj_goal=FollowJointTrajectoryGoal();
        self.point=JointTrajectoryPoint()
        
        
class schunk_bridge:
    def getPose(self,angles):         
        rospy.wait_for_service('schunk_fwd_kin')
        srv1 = rospy.ServiceProxy('schunk_fwd_kin', FwdKin_srv)
        rsp = srv1(angles)        
        pose=Pose()
        pose=rsp.pose;
        return pose;
    def getIK(self,pose):
        rospy.wait_for_service('schunk_inv_kin')
        srv2 = rospy.ServiceProxy('schunk_inv_kin', InvKin_srv)
        rsp = srv2(pose)                    
        return rsp.joint_angles;
    
    def js_cb(self,msg):
        self.joint_state.data=msg.position;
        return
        
         
    def __init__(self):
        rospy.init_node('schunk_bridge', anonymous=True)        
        self.sub1=rospy.Subscriber("/joint_states",JointState,self.js_cb)
        self.joint_state=Float64MultiArray()
        #self.joint_state.data=[0,0,0,0,0,0]
        
    def run(self):
        
        traj1=schunk_traj();
        traj1.create_action();        
        
        rospy.sleep(0.2)        
        pose1=self.getPose(self.joint_state);    
        print(pose1)    
        #pose1.position.x=0;
        #pose1.position.y=-0.6540;
        #pose1.position.z=0.614;        
        #pose1.orientation.w=0.707;
        #pose1.orientation.x=0.707;
        #pose1.orientation.y=0;
        #pose1.orientation.z=0;        
                
        #p=Float64MultiArray()
        #p.data=[1.24,1.1,1.5,-0.5,0.31,-0.4]

        #traj1.add_point(p,10)
        now=self.joint_state.data;
        t=1.0;                          
        for i in range(0,5):            
            pose1.position.x=pose1.position.x-0.05;
            #pose1.position.x=pose1.position.x+0.001;
            #q=tf.transformations.quaternion_from_euler(float(i)*0.0, 0, 0)
            #pose1.orientation=Quaternion(*q);
            diff=1000;    
            d=0;
            try:
                sols=self.getIK(pose1)
                for j in range(0,len(sols)):                                
                    #for k in range(0,5):                                                                                
                    #print(str(j)+ " : " + str(sols[j].data) + " " + str(d));                    
                    d=numpy.linalg.norm(numpy.array(now)-numpy.array(sols[j].data))
                    d=max(numpy.absolute(numpy.array(now)-numpy.array(sols[j].data)))
                    if d<diff:
                        diff=d;
                        best=sols[j]
                
                
                print("tb: " + str(t))
                t=t+(2.0*math.fabs(diff));
                print("B: (" + str(diff) + "): " + str(best.data) + "t: " + str(t));                
                #try:
			#		input("Press enter to continue")
            #    except SyntaxError:
		#			pass
                traj1.add_point(best,copy(t));
                now=best.data
                
            
            except rospy.ROSInterruptException:
                    print "Error in IK"
                
        
        traj1.call_action()
        
        
            
if __name__ == '__main__':
    bridge=schunk_bridge();
    bridge.run()
    rospy.spin()
    
