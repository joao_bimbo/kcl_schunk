#!/usr/bin/env python
import os
import rospy
import roslib
from numpy import array
roslib.load_manifest('kcl_schunk')
from std_msgs.msg import String
from std_msgs.msg import Float64MultiArray
from control_msgs.msg import FollowJointTrajectoryAction,FollowJointTrajectoryActionGoal,JointTolerance,FollowJointTrajectoryGoal
from trajectory_msgs.msg import JointTrajectoryPoint
from kcl_schunk.srv import FwdKin_srv,InvKin_srv 
from sensor_msgs.msg import JointState,Joy
from geometry_msgs.msg import Pose,PoseStamped
import tf

import actionlib
from copy import copy
import sensor_msgs
import numpy
import tf_conversions
from geometry_msgs.msg._Quaternion import Quaternion
import math
import ros
import code
import threading
import time

class schunk_traj:
    def create_action(self):                    
        self.traj_goal.trajectory.header.stamp=rospy.Time.now()
        self.traj_goal.trajectory.joint_names=['arm_1_joint', 'arm_2_joint', 'arm_3_joint', 'arm_4_joint', 'arm_5_joint', 'arm_6_joint'];
        self.tol = JointTolerance()    
        for i in range(0,6):
            self.tol.name=self.traj_goal.trajectory.joint_names[i];
            self.tol.acceleration=0.002
            self.tol.velocity=0.0002
            self.tol.position=0.0001    
            self.traj_goal.path_tolerance.append(copy(self.tol));
            self.traj_goal.goal_tolerance.append(copy(self.tol));
   
        
        return True
    
    def add_point(self,array_pose,time):
        a=JointTrajectoryPoint()
        
        self.point.positions=copy(array_pose.data)
        self.point.time_from_start=rospy.Duration.from_sec(self.t_next+time)
        self.t_next=self.t_next+time
        self.traj_goal.trajectory.points.append(copy(self.point))
        
        #self.traj_goal.path_tolerance.append(self.tol)
        return True;
    def clear_points(self):
        self.traj_goal.trajectory.points=[]
        self.traj_goal.path_tolerance=[];
        self.traj_goal.goal_tolerance=[];
        self.t_next=0
        return
		
    def call_action(self):        
        print(self.traj_goal.trajectory)        
        #self.act_client.wait_for_server()        
        now=rospy.Time.now()
        self.act_client.send_goal(self.traj_goal)  
        self.act_client.wait_for_result(rospy.Duration(0.1))  
        self.traj_goal.trajectory.header.stamp=rospy.Time.now()
 
        print("STATE")
        print(self.act_client.get_state())
        print("RESULT")   
        print(self.act_client.get_result())
        self.clear_points() 
        return       
        #self.act_client.wait_for_result(rospy.Duration.from_sec(10.0));        
       # print(str(self.act_client.get_result()) + " TIME: " + str((rospy.Time.now()-now).to_sec()))
        
        
    def __init__(self):
        self.act_client = actionlib.SimpleActionClient('/arm/joint_trajectory_controller/follow_joint_trajectory',FollowJointTrajectoryAction)
        #self.act_client = actionlib.SimpleActionClient('/arm_controller/follow_joint_trajectory',FollowJointTrajectoryAction)
        self.traj_goal=FollowJointTrajectoryGoal();
        self.point=JointTrajectoryPoint()
        self.t_next=0;
        
        
class schunk_joy:
    def array_to_pose(self,trans,rot):
        pose=Pose()
        pose.position.x=trans[0];
        pose.position.y=trans[1];
        pose.position.z=trans[2];
        pose.orientation.w=rot[3];
        pose.orientation.x=rot[0];
        pose.orientation.y=rot[1];
        pose.orientation.z=rot[2];
        return pose
        
        
    def getPose(self,angles):         
        rospy.wait_for_service('schunk_fwd_kin')
        srv1 = rospy.ServiceProxy('schunk_fwd_kin', FwdKin_srv)
        rsp = srv1(angles)        
        pose=Pose()
        pose=rsp.pose;
        print(pose)
        return pose;
    def getIK(self,pose):
        rospy.wait_for_service('schunk_inv_kin')        
        srv2 = rospy.ServiceProxy('schunk_inv_kin', InvKin_srv)
        try:
            rsp = srv2(pose)
            return rsp.joint_angles
        except rospy.ServiceException:
            print "Error in ik"
            stay=list()
            stay.append(self.joint_state)
            
            return stay
                                                    
    
    def js_cb(self,msg):
        self.joint_state.data=msg.position;
        return
    
    def cart_to_spher(self,xyz):
        r=numpy.linalg.norm(xyz);
        c=numpy.array([r, math.acos(xyz[2]/(r)),math.atan2(xyz[1],xyz[0])])
#         if(c[2]>math.pi):
#             c[0]=-c[0];
#             c[2]=c[2]-math-pi      
        return c
     
    def spher_to_cart(self,rtp):
            return numpy.array([rtp[0]*math.sin(rtp[1])*math.cos(rtp[2]), rtp[0]*math.sin(rtp[1])*math.sin(rtp[2]), rtp[0]*math.cos(rtp[1])])
    
    def pose_to_matrix(self,pose):        
        m = numpy.zeros(shape = (4,4))
        
        m=tf.transformations.quaternion_matrix([pose.orientation.x,pose.orientation.y,pose.orientation.z,pose.orientation.w])
        m[0:3,3]=numpy.array([pose.position.x,pose.position.y,pose.position.z])        
        
        return m;
    
    def spher_rot_mat(self,rtp):
        m = numpy.zeros(shape = (3,3))
        #m = [[math.sin(rtp[1])*math.cos(rtp[2]), math.cos(rtp[1])*math.cos(rtp[2]),-math.sin(rtp[2])],[math.sin(rtp[1])*math.sin(rtp[2]), math.cos(rtp[1])*math.sin(rtp[2]), math.cos(rtp[2])],[math.cos(rtp[1]), -math.sin(rtp[1]), 0]];
        #m = [[math.sin(rtp[2])*math.cos(rtp[1]), math.cos(rtp[1])*math.cos(rtp[1]),-math.sin(rtp[1])],[math.sin(rtp[2])*math.sin(rtp[1]), math.cos(rtp[2])*math.sin(rtp[1]), math.cos(rtp[1])],[math.cos(rtp[2]), -math.sin(rtp[2]), 0]];
        m = [[math.cos(rtp[1]), math.sin(rtp[1])*math.cos(rtp[2]),math.sin(rtp[1])*math.sin(rtp[2])],[-math.sin(rtp[1]), math.cos(rtp[1])*math.cos(rtp[2]), math.cos(rtp[1])*math.sin(rtp[2])],[0, -math.sin(rtp[2]), math.cos(rtp[1])]];
        return m
    def wait(self):
        try:
            input("Press enter to continue")
        except SyntaxError:
            pass
    
    def axis_angle(self,a,b):
        a1=[a[0,0], a[0,1], a[0,2]]
        b1=[b[0], b[1], b[2]]
        cp=numpy.cross(a1, b1)
        angle=numpy.dot(a1,b1)
        qx = cp[0] * math.sin(angle/2)
        qy = cp[1] * math.sin(angle/2)
        qz = cp[2] * math.sin(angle/2)
        qw = math.cos(angle/2)
        q=[qw,qx,qy,qz]
        return q
        

        
        
    def align_vectors(self,a,b):
        #http://math.stackexchange.com/questions/180418/calculate-rotation-matrix-to-align-vector-a-to-vector-b-in-3d
        I=numpy.identity(3)
        
        a1=[a[0,0], a[0,1], a[0,2]]
        b1=[b[0], b[1], b[2]]
        v=numpy.cross(a1, b1)
        
        vx = numpy.zeros(shape = (3,3))

        vx=numpy.matrix([[0, -v[2], v[1]],[v[2], 0, -v[0]],[-v[1], v[0], 0]])
        vx=array([[0, -v[2], v[1]],[v[2], 0, -v[0]],[-v[1], v[0], 0]])
      
#        aux1 = numpy.zeros(shape = (3,3))
#         for i in range(0,3):
#             for j in range(0,3):
#                 aux1[i,j]=vx[i,j]*vx[i,j]
#                 
        aux1=vx*vx     

        s=numpy.linalg.norm(v)
        c=numpy.dot(a,b)        
        aux2=1-c/(s*s)
        
        res=I+vx+(aux2[0,0]*numpy.matrix(aux1))
        resn = numpy.zeros(shape = (3,3))
        aa = numpy.zeros(shape = (3))
        for i in range(0,3):
            n=numpy.linalg.norm(res[:,i])
            for j in range(0,3):
                resn[j,i]=res[j,i]/n
  
        return resn;

    def move_cartesian(self,d):
        cur_pose=self.getPose(self.int_joint_state)
        T_ee_r=numpy.matrix(self.pose_to_matrix(cur_pose))
        delta_pose=tf.transformations.euler_matrix(d[4]*self.deltaa,d[5]*self.deltaa, d[6]*self.deltaa, 'sxyz')
            
        Pee_b=T_ee_r
        Pee_b[0:3,0:3]=delta_pose[0:3,0:3]*T_ee_r[0:3,0:3]
            
            
        Pee_b[0:3,3]=numpy.transpose(numpy.matrix([T_ee_r[0,3]+d[1]*self.deltar, T_ee_r[1,3]+d[2]*self.deltar, T_ee_r[2,3]+d[3]*self.deltar]))
            
            
            
        q_next=tf.transformations.quaternion_from_matrix(Pee_b);
        new_pose=self.array_to_pose([Pee_b[0,3], Pee_b[1,3], Pee_b[2,3]], q_next)
            
        jangles=self.getIK_best(new_pose)
            
            
            
       # print("############")
       # print(self.joint_state.data)
       # print(jangles)
            
            #traj=schunk_traj();
            #traj.create_action();                
        diff=(numpy.linalg.norm(array(self.int_joint_state.data)-array(jangles.data)))
            
            
            
        if(diff<1):
            self.traj.add_point(jangles, 1*self.speed)
            self.int_joint_state=jangles;            
            self.traj.call_action()
        else:
            print("ERROR! Max. speed exceeded")
            
        return
        
        
    def joy_cb(self,msg):
        
        b1=msg.buttons[4]; #10
        b2=msg.buttons[5]; #11
        bs=msg.buttons[9];
        
        
        
        
        
       
        d1=msg.axes[0];
        d2=msg.axes[1];
        d3=msg.axes[2];
        d4=msg.axes[3];
        d5=0;
        d6=0;
        
        d=array([0,d1,d2,d3,d4,d5,d6]);




        
        if bs==1:
			rospy.sleep(0.1)
          #  self.goto_rcm()

        if b1==1:
            cur_pose=self.getPose(self.joint_state)
            iTb_r=numpy.linalg.inv(self.Tb_r)
         
            Tee_b=self.pose_to_matrix(cur_pose)
            T_ee_r=numpy.matrix(iTb_r)*numpy.matrix(Tee_b);
            cur_sph=self.cart_to_spher(T_ee_r[0:3,3])
            
            #Do changes according to joystick
            s=numpy.sign(cur_sph[0]);
            s=1                                  
            cur_sph[0]=cur_sph[0]+(s*d1*self.deltar);
            s=numpy.sign(cur_sph[1]);
            cur_sph[1]=cur_sph[1]+(s*d2*self.deltaa);
            s=numpy.sign(cur_sph[2]);                        
            cur_sph[2]=cur_sph[2]+(s*d3*self.deltaa);            
            xyz=self.spher_to_cart(cur_sph)            
                        
            v1=[-T_ee_r[0,2],-T_ee_r[1,2],-T_ee_r[2,2]];
            #v1=v1/numpy.linalg.norm(v1)
            v2=xyz/numpy.linalg.norm(-xyz)
            q=self.quat_between_vectors(v1,v2)
            #new_pose_r=self.array_to_pose(xyz, q)
            
            Trr=tf.transformations.quaternion_matrix(q)            
        
            Tfinal_r=Trr*T_ee_r
            
            
            #Tfinal_r=Trr2*T_ee_r;
          
            #Tfinal_r[0:3,3]=numpy.transpose(numpy.matrix(xyz[0:3]))
            
            xyz2=Tfinal_r[0:3,0:3]*numpy.transpose(numpy.matrix([0,0,-cur_sph[0]]))
            
            Tfinal_r[0:3,3]=xyz2[0:3]
            
            Trr2=tf.transformations.euler_matrix(d4*self.deltaa,d5*self.deltaa, 0, 'rxyz')
            Tfinal2_r=Trr2*Tfinal_r;
            
            Pee_b=self.Tb_r*Tfinal2_r;
            
            q_next=tf.transformations.quaternion_from_matrix(Pee_b);
            new_pose=self.array_to_pose([Pee_b[0,3], Pee_b[1,3], Pee_b[2,3]], q_next)
            
                        
            jangles=self.getIK_best(new_pose)
            n_ang=array(jangles.data)
            n_ang[5]=n_ang[5]+d6*self.deltaa;
           
            jangles.data=n_ang;
          #  traj=schunk_traj();
          #  traj.create_action();
            
#           if(numpy.linalg.norm(numpy.array(jangles.data)-numpy.array(self.joint_state.data)))<0.5:
                #print("LIMITS EXCEEDED")
                
            diff=(numpy.linalg.norm(array(self.joint_state.data)-n_ang))
            
            if(diff<1):
                self.traj.add_point(jangles, 1*self.speed)
             #   traj.call_action()
            else:
                print("ERROR! Max. speed exceeded")
            

        
        if b2==1:
            print(self.joint_state.data)
            self.move_cartesian(d)
          
                    
##===============================================================================
##   This was the previous thing to move in spherical coordinates. Was combined with previous
#         if msg.buttons[4]==1:
#             #   print(T_ee_r[0:3,3])
#             cur_pose=self.getPose(self.joint_state)
#             iTb_r=numpy.linalg.inv(self.Tb_r)
#            
#             Tee_b=self.pose_to_matrix(cur_pose)
#             T_ee_r=numpy.matrix(iTb_r)*numpy.matrix(Tee_b);
#             #print("Terr")
#             #print(T_ee_r)
#             #print(self.Tb_r*T_ee_r)
#             cur_sph=self.cart_to_spher(T_ee_r[0:3,3])
#             #Do changes according to joystick
#               
#               
#               
#             s=numpy.sign(cur_sph[0]);                                  
#             cur_sph[0]=cur_sph[0]+(s*msg.axes[3]*self.deltar);
#               
#             #cur_sph[1]=cur_sph[1]+msg.axes[0]*self.deltaa;                        
#             #cur_sph[2]=cur_sph[2]+msg.axes[1]*self.deltaa;            
#                                  
#             Trr2=tf.transformations.euler_matrix(msg.axes[0]*self.deltaa,msg.axes[1]*self.deltaa, 0, 'sxyz')
#             Tfinal_r=Trr2*T_ee_r
#               
#             xyz2=Tfinal_r[0:3,0:3]*numpy.transpose(numpy.matrix([0,0,-cur_sph[0]]))
#             Tfinal_r[0:3,3]=xyz2[0:3]
#             Pee_b=self.Tb_r*Tfinal_r;
#               
#               
#             q_next=tf.transformations.quaternion_from_matrix(Pee_b);
#             new_pose=self.array_to_pose([Pee_b[0,3], Pee_b[1,3], Pee_b[2,3]], q_next)
#               
#                         #code.interact(local=locals())
#   
#               
#             jangles=self.getIK_best(new_pose)
#             n_ang=array(jangles.data)
#             n_ang[5]=n_ang[5]+msg.axes[4]*self.deltaa;
#             jangles.data=n_ang;
#             traj=schunk_traj();
#             traj.create_action();
#               
# #           if(numpy.linalg.norm(numpy.array(jangles.data)-numpy.array(self.joint_state.data)))<0.5:
#                 #print("LIMITS EXCEEDED")            
#             diff=(numpy.linalg.norm(array(self.joint_state.data)-n_ang))
#               
#             if(diff<1):
#                 traj.add_point(jangles, 0.1)
#                 traj.call_action()
#             else:
#                 print("ERROR! Max. speed exceeded")
#             
#===============================================================================
        

            
                
                
                
                
        #=======================================================================
        # success=False
        # while not success:            
        #     try:
        #         while not self.listener.frameExists('RCM'):
        #             print('.')
        #             pass
        #         rospy.sleep(0.1)                
        #         #t = self.listener.getLatestCommonTime('/RCM','/ik_frame')
        #         self.listener.waitForTransform("/RCM", "/ik_frame", rospy.Time.now(), rospy.Duration(2.0))                        
        #         cur_pose_stamped.header.stamp=rospy.Time.now();               
        #         cur_pose_stamped=self.transf.transformPose("/RCM", cur_pose_stamped)
        #         success=True
        #         
        #     except rospy.ROSInterruptException:
        #         print "Error in tf joy_cb"
        #         continue
        #=======================================================================
            
    def quat_between_vectors(self,v1,v2):        
        axis=numpy.cross(v1,v2)        
        q=[axis[0],axis[1], axis[2], math.sqrt(
                                               (v1[0]*v1[0]+v1[1]*v1[1]+v1[2]*v1[2])
                                               *
                                               (v1[0]*v1[0]+v1[1]*v1[1]+v1[2]*v1[2])
                                               +
                                               (v1[0]*v2[0]+v1[1]*v2[1]+v1[2]*v2[2]))]
                   
        q=q/numpy.linalg.norm(q)
        
        #q.xyz = a;
        #q.w = sqrt((v1.Length ^ 2) * (v2.Length ^ 2)) + dotproduct(v1, v2)
        return q
        
    def goto_rcm(self):
        try:    
            while not self.listener.frameExists('ik_frame'):
                pass
            
            t = self.listener.getLatestCommonTime('/ik_frame','/RCM')
        #    now=rospy.Time.now()
        #    self.listener.waitForTransform("/RCM", "/ik_frame", t, rospy.Duration(2.0))                        
            (trans,rot) = self.listener.lookupTransform('/ik_frame','/RCM', t)
            self.rcm_pose=self.array_to_pose(trans,rot)
            self.Tb_r=numpy.matrix(self.transf.fromTranslationRotation(trans, rot))            
        
            
            Pee_r=tf.transformations.euler_matrix(0, math.pi, 0, axes='sxyz')
            Pee_r[0:3,3]=numpy.array([0,0,0.2])
            
            Pee_b=self.Tb_r*Pee_r;
            q_initial=tf.transformations.quaternion_from_matrix(Pee_b);
            initial_pose=self.array_to_pose([Pee_b[0,3], Pee_b[1,3], Pee_b[2,3]], q_initial)
                        
            print(initial_pose)

            #initial_pose.position.x=initial_pose.position.x
            jangles=self.getIK_best(initial_pose)
            traj=schunk_traj();
            if(numpy.linalg.norm(array(self.joint_state.data)-array(jangles.data))>0.001):                
                traj.create_action();    
                t=(numpy.linalg.norm(array(self.joint_state.data)-array(jangles.data)))
                traj.add_point(jangles, t/self.speed)
                traj.call_action()
                traj.act_client.wait_for_result(rospy.Duration.from_sec(0.0));
                while numpy.linalg.norm(array(self.joint_state.data)-array(jangles.data))>0.001:
                    rospy.sleep(0.1)
                
                self.done=True               
            os.system('play --no-show-progress --null --channels 1 synth %s sine %f' % ( 0.1, 410))
        
           
        except rospy.ROSInterruptException:
            print "Error in tf ik_frame to RCM"
            return

        
        
    def getIK_best(self,pose):
        now=self.joint_state.data;
        diff=1000;
        best=self.joint_state
        try:
            sols = self.getIK(pose)
            for j in range(0, len(sols)):
                    #d = numpy.linalg.norm(numpy.array(now) - numpy.array(sols[j].data))
                    #maximum distance travelled by joint
                    sol=numpy.array(sols[j].data)
                    d = max(numpy.absolute(numpy.array(now) - sol))
                    #away from joint limits
                    if(max(numpy.absolute([sol[1],sol[3],sol[5]]))>2.08439):
                        d=1000;
                    if d < diff:
                        diff = d;
                        best = sols[j]            
            #print(sols)
            if(len(sols)>0):
                return best
            else:
                return false
            
        except rospy.ROSInterruptException:
                    print "Error in IK"
        

         
    def __init__(self):
        rospy.init_node('schunk_rcm_joy', anonymous=True)   
        self.listener = tf.TransformListener()     
        self.transf=tf.TransformerROS()
        self.sub1=rospy.Subscriber("/joint_states",JointState,self.js_cb)
        self.sub2=rospy.Subscriber("/joy",Joy,self.joy_cb, queue_size=1)
        self.deltar=0.01
        self.deltaa=0.1
        self.speed=0.1;        
        self.traj=schunk_traj();
        self.traj.create_action();

        self.joint_state=Float64MultiArray()
        #self.joint_state.data=[0,0,0,0,0,0]
      #  rospy.sleep(0.2)        

        
    def run(self):
        #while not rospy.is_shutdown():
        r = rospy.Rate(2)
        self.traj.add_point(self.joint_state,0.1)
        while not rospy.is_shutdown():
            r.sleep()
            if(len(self.traj.traj_goal.trajectory.points)>1):
                self.traj.clear_points()
                            
           #     self.traj.call_action()
                
                self.traj.add_point(self.joint_state,0.1)
            
            self.int_joint_state=self.joint_state
            
            
	#		rospy.sleep(0.5)  
			 		
		
		#second=self.joint_state;     
		#second_point=array(self.joint_state.data)
		#print(second_point)
		#second_point[3]=second_point[3]-0.1
		#second_point[4]=second_point[4]+0.2
		#second.data=second_point;
        #self.traj.add_point(second,1)
        
        
        

       # self.goto_rcm()
        
                    
        
     #   traj1.call_action()
        
        
            
if __name__ == '__main__':
    joy_node=schunk_joy();
    #threading.Thread(target=joy_node.run).start()
    joy_node.run()
    
    numpy.set_printoptions(precision=4,linewidth=999)
#    while joy_node.done==False:
#        rospy.sleep(0.1)
#    joy_node.sub2=rospy.Subscriber("/joy",Joy,joy_node.joy_cb)    
    rospy.spin()
    
