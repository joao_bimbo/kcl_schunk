#include <stdio.h>
#include "ikfast.h"
#include "schunk_ikfast_61.cpp"
#include <kcl_schunk/InvKin_srv.h>
#include <kcl_schunk/FwdKin_srv.h>
#include <ros/ros.h>
#include <tf/tf.h>
#include <tf/LinearMath/Matrix3x3.h>
#include <tf_conversions/tf_eigen.h>


using namespace Eigen;


#define IKREAL_TYPE IkReal

class Schunk_Kinematics{

protected:
    ros::NodeHandle n;
    ros::ServiceServer srv_fwd_kin,srv_inv_kin;
    IKSolver ik_solver;

public:
    Schunk_Kinematics(int argc, char **argv) : n() {

        srv_fwd_kin=n.advertiseService("schunk_fwd_kin", &Schunk_Kinematics::fwd_kin,this);
        srv_inv_kin=n.advertiseService("schunk_inv_kin", &Schunk_Kinematics::inv_kin,this);

    }

    void run(){
        ros::spin();
    }

    bool fwd_kin(kcl_schunk::FwdKin_srvRequest& request,kcl_schunk::FwdKin_srvResponse& response){

        if(request.joint_angles.data.size() != 6) {
            ROS_ERROR("Invalid joint_angles size (DOF=6)");
            return false;
        }
        IkReal joints[9];
        for(int i=0;i<6;i++)
            joints[i]=request.joint_angles.data[i];

        IkReal eetrans[3];
        IkReal eerot[9];

        ComputeFk(joints,eetrans,eerot);

        tf::Matrix3x3 m = tf::Matrix3x3(
                (tfScalar)eerot[0],(tfScalar)eerot[1],(tfScalar)eerot[2],
                (tfScalar)eerot[3],(tfScalar)eerot[4],(tfScalar)eerot[5],
                (tfScalar)eerot[6],(tfScalar)eerot[7],(tfScalar)eerot[8]);

        tf::Quaternion q;

        m.getRotation(q);
        tf::quaternionTFToMsg(q,response.pose.orientation);
        response.pose.position.x=(double) eetrans[0];
        response.pose.position.y=(double) eetrans[1];
        response.pose.position.z=(double) eetrans[2];



        return true;
    }


    bool inv_kin(kcl_schunk::InvKin_srvRequest& request,kcl_schunk::InvKin_srvResponse& response){

        // IkSolutionListBase<IKREAL_TYPE> solutions;


        IkReal eerot[9];
        IkReal eetrans[3];

        tf::Matrix3x3 m;
        tf::Quaternion q;
        tf::quaternionMsgToTF(request.pose.orientation, q);
        m.setRotation(q);

        eetrans[0]=request.pose.position.x;
        eetrans[1]=request.pose.position.y;
        eetrans[2]=request.pose.position.z;

        eerot[0]=m[0][0];
        eerot[1]=m[0][1];
        eerot[2]=m[0][2];
        eerot[3]=m[1][0];
        eerot[4]=m[1][1];
        eerot[5]=m[1][2];
        eerot[6]=m[2][0];
        eerot[7]=m[2][1];
        eerot[8]=m[2][2];


        Matrix3d mm;
        for(int i=0;i<3;i++){
        for(int j=0;j<3;j++){
            mm(i,j)=m[i][j];
        }}


        IkSolutionList<IkReal> solutions;

        bool bSuccess = ComputeIk(eetrans, eerot, NULL, solutions);
        if(!bSuccess) {

        ROS_ERROR_STREAM("Invalid/non-reachable pose " << request.pose.orientation << "," << request.pose.position);
            return false;
        }

        std_msgs::Float64MultiArray sol_f;

        std::vector<IkReal> solvalues(6);

        for(size_t i=0;i<solutions.GetNumSolutions();i++){

            const IkSolutionBase<IkReal>& sol = solutions.GetSolution(i);
            sol.GetSolution(&solvalues[0],0);
            for (size_t j=0;j<6;j++){
                sol_f.data.push_back(solvalues[j]);
                //Q(solvalues[j]);

            }
            response.joint_angles.push_back(sol_f);
            sol_f.data.clear();
        }

        return true;
    }

};

int main(int argc, char **argv){
    ros::init(argc, argv, "kcl_schunk_node");
    Schunk_Kinematics kin(argc,argv);
    kin.run();


    return 0;
}
